## 12.0.0 (2021-03-17)

### Funkčnosti

- remove .versionrc [#5](https://gitlab.actis.cz/actisdevs/exevido/-/issues/5)
- **YML:** update template [#54](https://gitlab.actis.cz/actisdevs/exevido/-/issues/54)
- edit template [#53](https://gitlab.actis.cz/actisdevs/exevido/-/issues/53)
- changelog reset [#52](https://gitlab.actis.cz/actisdevs/exevido/-/issues/52)
- even more files 
- more files 
- yyy1 

### Opravy

- remove extras [#54](https://gitlab.actis.cz/actisdevs/exevido/-/issues/54)
- fix path? 
- yyy2
